<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UserLoginRequest;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\JsonResponse;
use Illuminate\Http\JsonResponse as Response;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function login(UserLoginRequest $request): Response
    {
        if (!$token = JWTAuth::attempt($request->validated())) {
            return new ErrorResponse(['message' => 'user_not_found'], ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->respondWithToken($token);
    }

    public function logout(): Response
    {
        auth()->logout();

        return new JsonResponse();
    }
}
