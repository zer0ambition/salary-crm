<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse as HttpJsonResponse;
use InvalidArgumentException;

class JsonResponse extends HttpJsonResponse
{
    /**
     * Sets the data to be sent as JSON.
     *
     * @param mixed $data Incoming data
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setData($data=[]) : self
    {
        $response = [
            'status'  => $this->isOk(),
            'code' => $this->statusCode
        ];

        if (isset($data['errors']) && !empty($data['errors'])) {
            $response['errors'] = $data['errors'];
        }

        if (isset($data['message']) && !empty($data['message'])) {
            $response['message'] = $data['message'];
        }

        if (isset($data['data']) && !empty($data['data'])) {
            $response['data'] = $data['data'];
        }

        $this->data = json_encode($response);

        if (! $this->hasValidJson(json_last_error())) {
            throw new InvalidArgumentException(json_last_error_msg());
        }

        return $this->update();
    }
}
